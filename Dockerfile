FROM alpine:3.7

LABEL maintainer "https://github.com/blacktop"

RUN apk add --no-cache openjdk8-jre su-exec bash

ENV VERSION 6.2.4
ENV URL "https://artifacts.elastic.co/downloads/logstash"
ENV TARBALL "$URL/logstash-${VERSION}.tar.gz"
ENV TARBALL_ASC "$URL/logstash-${VERSION}.tar.gz.asc"
ENV TARBALL_SHA "08cc01f1dead7d0afcd2c5a70cc5e12d52ed0668764465703f772e3885e732d47db19805160cc0d647833349808341a0d23b595bbfd8a007ea55aceaccfe0a30"
ENV GPG_KEY "46095ACC8548582C1A2699A9D27D666CD88E42B4"

RUN apk add --no-cache libzmq bash
RUN apk add --no-cache -t .build-deps wget ca-certificates gnupg openssl \
  && set -ex \
  && cd /tmp \
  && wget --progress=bar:force -O logstash.tar.gz "$TARBALL"; \
  if [ "$TARBALL_SHA" ]; then \
  echo "$TARBALL_SHA *logstash.tar.gz" | sha512sum -c -; \
  fi; \
  \
  if [ "$TARBALL_ASC" ]; then \
  wget --progress=bar:force -O logstash.tar.gz.asc "$TARBALL_ASC"; \
  export GNUPGHOME="$(mktemp -d)"; \
  ( gpg --keyserver ha.pool.sks-keyservers.net --recv-keys "$GPG_KEY" \
  || gpg --keyserver pgp.mit.edu --recv-keys "$GPG_KEY" \
  || gpg --keyserver keyserver.pgp.com --recv-keys "$GPG_KEY" ); \
  gpg --batch --verify logstash.tar.gz.asc logstash.tar.gz; \
  rm -rf "$GNUPGHOME" logstash.tar.gz.asc || true; \
  fi; \
  tar -xzf logstash.tar.gz \
  && mv logstash-$VERSION /usr/share/logstash \
  && adduser -DH -s /sbin/nologin logstash \
  && rm -rf /tmp/* \
  && apk del --purge .build-deps

RUN apk add --no-cache libc6-compat rsync

ENV PATH /usr/share/logstash/bin:/sbin:$PATH
ENV LS_SETTINGS_DIR /usr/share/logstash/config
ENV LANG='en_US.UTF-8' LC_ALL='en_US.UTF-8'

RUN set -ex; \
  if [ -f "$LS_SETTINGS_DIR/log4j2.properties" ]; then \
  cp "$LS_SETTINGS_DIR/log4j2.properties" "$LS_SETTINGS_DIR/log4j2.properties.dist"; \
  truncate -s 0 "$LS_SETTINGS_DIR/log4j2.properties"; \
  fi

VOLUME ["/usr/share/logstash/config", "/usr/share/logstash/pipeline"]

RUN  mkdir -p /.backup/logstash/config/
RUN mkdir /.backup/logstash/pipeline/

COPY config/logstash /.backup/logstash/config
COPY config/pipeline /.backup/logstash/pipeline
RUN chown -R logstash /.backup/logstash
COPY restore-config.sh /run/
COPY logstash-entrypoint.sh /

RUN /usr/share/logstash/bin/logstash-plugin install logstash-input-cloudwatch
RUN /usr/share/logstash/bin/logstash-plugin install logstash-filter-json_encode
RUN /usr/share/logstash/bin/logstash-plugin install logstash-filter-translate
RUN /usr/share/logstash/bin/logstash-plugin install logstash-codec-cloudtrail
RUN /usr/share/logstash/bin/logstash-plugin install logstash-input-s3
RUN /usr/share/logstash/bin/logstash-plugin update logstash-output-kafka

EXPOSE 9600 3520-3529 1516 1516/udp

ENV LOGSTASH_PWD="changeme" \
    ELASTICSEARCH_HOST="es-client-1" \
    ELASTICSEARCH_PORT="9200" \
    HEAP_SIZE="1g" \
    TS_PWD="changeme" \
    RABBITMQ_USER="admin" \
    RABBITMQ_PASSWORD="changeme"

ENTRYPOINT ["/logstash-entrypoint.sh"]
CMD ["-f", "/usr/share/logstash/pipeline", "-r"]
